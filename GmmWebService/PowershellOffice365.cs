﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security;
using System.Runtime.InteropServices;
using System.Collections;
using System.Text;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

namespace GmmWebService
{
    public class PowershellOffice365
    {        
        private string userPS;
        private string passPS;
        private string pathPS;
        private string keyPS;
        private string userPSOnPremises;
        private const string connectionUri = "https://ps.outlook.com/powershell";
        private readonly string uriServidorOnPremises; // = "http://EMLGERPM99.gerdau.net/powershell?serializationLevel=Full";
        private const string connectionUriO3665WithExchange = "https://outlook.office365.com/powershell-liveid/";
        private const string connectionUriOnPremises = "http://schemas.microsoft.com/powershell/Microsoft.Exchange";

        public PowershellOffice365(string user, string pass, string path, string key, string userOnPremises, string servidorOnPremises)
        {
            userPS = user;
            passPS = pass;
            pathPS = path;
            keyPS = key;
            userPSOnPremises = userOnPremises;
            uriServidorOnPremises = $"{servidorOnPremises}/powershell?serializationLevel=Full";
        }

        #region Public methods

        public List<string> GetUserEmails(string identity)
        {
            List<string> emails = new List<string>();
            SecureString secpassword = null;
            List<ErrorRecord> errors;

            secpassword = GetSecureString();
            PSCredential credential = new PSCredential(userPSOnPremises, secpassword);
            PSCommand cmd = new PSCommand();

            cmd.AddCommand("Get-RemoteMailbox");
            cmd.AddParameter("Identity", identity);

            var result = ExecutePowerShellCommand(cmd, uriServidorOnPremises, credential, out errors);

            if (result.Count > 0 && result[0].Members["EmailAddresses"] != null)
            {
                var list = ((ArrayList)(((PSObject)result[0].Members["EmailAddresses"].Value).BaseObject));
                var primaryAddress = "";

                foreach (var email in list)
                {
                    var item = (string)email;

                    if (item.StartsWith("SMTP:"))
                    {
                        primaryAddress = item;
                    }
                    else if (item.StartsWith("smtp:"))
                    {
                        emails.Add(item);
                    }
                }

                emails.Add(primaryAddress);
                emails.Reverse();
            }

            return emails;
        }

        public bool CreateMailbox(string login, string email, string domain, List<string> outrosEmails, out string errorMessage)
        {
            SecureString secpassword = null;
            List<ErrorRecord> errors;
            errorMessage = string.Empty;
            bool success = true;

            if (string.IsNullOrEmpty(login))
            {
                errorMessage = "Login must be provided.";
                return false;
            }

            if (string.IsNullOrEmpty(email))
            {
                errorMessage = "E-mail must be provided.";
                return false;
            }

            if (string.IsNullOrEmpty(domain))
            {
                errorMessage = "Domain must be provided.";
                return false;
            }

            try
            {
                string primaryEmail = string.Format("{0}@{1}", email, domain);
                //string cldEmail = string.Format("{0}@gerdaucld.mail.onmicrosoft.com", login);
                //string intEmail = string.Format("{0}@int.{1}", login, domain);
                string cldEmail = BuildCldEmailAddress(login);
                string intEmail = BuildIntEmailAddress(login, domain);

                var existsEmail = ExistsIntEmailAddress(intEmail);

                if (existsEmail)
                {
                    errorMessage = string.Format("E-mail {0} already exists.", intEmail);
                    return false;
                }

                existsEmail = ExistsCldEmailAddress(cldEmail);

                if (existsEmail)
                {
                    errorMessage = string.Format("E-mail {0} already exists.", cldEmail);
                    return false;
                }

                login = login.ToLower();
                PSCommand command1 = new PSCommand();
                secpassword = GetSecureString();
                PSCredential credential = new PSCredential(userPSOnPremises, secpassword);

                command1 = new PSCommand();
                command1.AddCommand("Enable-RemoteMailBox");
                command1.AddParameter("Identity", login);
                command1.AddParameter("RemoteRoutingAddress", cldEmail);
                command1.AddParameter("PrimarySmtpAddress", primaryEmail);

                //ProxyAddressCollection
                Hashtable emailAddressInt = new Hashtable();
                emailAddressInt.Add("add", intEmail);
                PSCommand command2 = new PSCommand();
                command2.AddCommand("Set-RemoteMailbox");
                command2.AddParameter("Identity", primaryEmail);
                command2.AddParameter("EmailAddresses", emailAddressInt);

                Hashtable emailAddressCld = new Hashtable();
                emailAddressCld.Add("add", cldEmail);
                PSCommand command3 = new PSCommand();
                command3.AddCommand("Set-RemoteMailbox");
                command3.AddParameter("Identity", primaryEmail);
                command3.AddParameter("EmailAddresses", emailAddressCld);

                var result = ExecutePowerShellCommandList(primaryEmail, command1, command2, command3, uriServidorOnPremises, credential, outrosEmails, out errors);

                if (errors != null && errors.Count > 0)
                {
                    foreach (var item in errors)
                    {
                        errorMessage += " " + item.ToString();
                    }

                    success = false;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                success = false;
            }

            return success;
        }

        public bool UpdateEmailList(string identity, List<string> emailList, out string updateMessage, out string errorMessage)
        {
            updateMessage = "";
            errorMessage = "";
            var success = false;
            var commandList = new List<PSCommand>();
            PSCommand command = null;
            Hashtable emailAddressParam = null;
            string primaryEmail = "";
            var emailsToAdd = new List<string>();
            var emailsToRemove = new List<string>();

            try
            {
                SplitUpdateEmailList(identity, emailList, out primaryEmail, out emailsToAdd, out emailsToRemove);

                foreach (var email in emailsToRemove)
                {
                    command = new PSCommand();
                    emailAddressParam = new Hashtable();
                    emailAddressParam.Add("remove", email);
                    command.AddCommand("Set-RemoteMailbox");
                    command.AddParameter("Identity", primaryEmail);
                    command.AddParameter("EmailAddresses", emailAddressParam);
                    commandList.Add(command);
                }

                List<ErrorRecord> errors = null;
                Collection<PSObject> result = null;

                if (commandList.Count > 0)
                {
                    result = ExecutePowerShellCommandList(commandList, out errors); 
                }

                if (errors != null && errors.Count > 0)
                {
                    foreach (var item in errors)
                    {
                        errorMessage += " " + item.ToString();
                    }

                    success = false;
                }
                else
                {
                    commandList = new List<PSCommand>();
                    foreach (var email in emailsToAdd)
                    {
                        command = new PSCommand();
                        emailAddressParam = new Hashtable();
                        emailAddressParam.Add("add", email);
                        command.AddCommand("Set-RemoteMailbox");
                        command.AddParameter("Identity", primaryEmail);
                        command.AddParameter("EmailAddresses", emailAddressParam);
                        commandList.Add(command);
                    }

                    if (commandList.Count > 0)
                    {
                        result = ExecutePowerShellCommandList(commandList, out errors); 
                    }

                    if (errors != null && errors.Count > 0)
                    {
                        foreach (var item in errors)
                        {
                            errorMessage += " " + item.ToString();
                        }

                        success = false;
                    }
                    else
                    {
                        if (emailsToAdd.Count > 0)
                        {
                            updateMessage = "Added e-mails: " + string.Join(",", emailsToAdd.ToArray());
                        }

                        if (emailsToRemove.Count > 0)
                        {
                            updateMessage = "Removed e-mails: " + string.Join(",", emailsToRemove.ToArray());
                        }

                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return success;
        }

        public Collection<PSObject> RemoveMailboxInExchangeOnPremises(string identity, out List<ErrorRecord> errors)
        {
            var cmd = new PSCommand();
            cmd.AddCommand("Disable-RemoteMailbox");
            cmd.AddParameter("Identity", identity);
            cmd.AddParameter("Force", new SwitchParameter(true));

            var results = ExecutePowerShellCommand(cmd, out errors);

            return results;
        }

        #region SendAs
        public List<PsSendAsItem> GetSendAsList(string login)
        {
            var command = new PSCommand();
            command.AddCommand("Get-RecipientPermission");
            command.AddParameter("Identity", login);

            var usuarios = ExecutePowershellOffice365WithExchange(command);  // ExecuteCommandPowershellOffice365(command);
            var resultList = new List<PsSendAsItem>();

            foreach (var obj in usuarios)
            {
                resultList.Add(new PsSendAsItem
                {
                    Trustee = obj.Members["Trustee"]?.Value as string,
                    AccessRights = obj.Members["AccessRights"]?.Value.ToString(),
                    AccessControlType = obj.Members["AccessControlType"]?.Value.ToString(),
                    IsInherited = (bool)obj.Members["IsInherited"].Value,
                    Identity = obj.Members["Identity"]?.Value as string
                });
            }

            return resultList;
        }

        public void AddSendAs(string login, string loginSendAs)
        {
            var cmd = new PSCommand();

            cmd.AddCommand("Add-RecipientPermission");
            cmd.AddParameter("Identity", login);
            cmd.AddParameter("AccessRights", "SendAs");
            cmd.AddParameter("Trustee", loginSendAs);
            cmd.AddParameter("Confirm", new SwitchParameter(false));

            ExecutePowershellOffice365WithExchange(cmd);
        }

        public void RemoveSendAs(string login, string loginSendAs)
        {
            var cmd = new PSCommand();

            cmd.AddCommand("Remove-RecipientPermission");
            cmd.AddParameter("Identity", login);
            cmd.AddParameter("AccessRights", "SendAs");
            cmd.AddParameter("Trustee", loginSendAs);
            cmd.AddParameter("Confirm", new SwitchParameter(false));

            ExecutePowershellOffice365WithExchange(cmd);
        }

        #endregion SendAs

        #region MailBoxRights
        public List<PsMailBoxRightsItem> ListMailboxRights(string login)
        {
            var resultList = new List<PsMailBoxRightsItem>();

            var command = new PSCommand();
            command.AddCommand("Get-MailboxPermission");
            command.AddParameter("Identity", login);

            var usuarios = ExecutePowershellOffice365WithExchange(command);
            string accessRights;

            foreach (var item in usuarios)
            {
                accessRights = item.Members["AccessRights"].Value.ToString();
                resultList.Add(new PsMailBoxRightsItem
                {
                    User = item.Members["User"].Value as string,
                    AccessRights = accessRights,
                    Deny = item.Members["Deny"].Value.ToString(),
                    HasFullAccess = (accessRights != null) && accessRights.Contains("FullAccess") ? true : false,
                    IsInherited = (bool)item.Members["IsInherited"].Value,
                    Identity = item.Members["Identity"]?.Value as string
                });
            }

            return resultList;
        }

        public void AddMailboxRights(string login, string loginMailboxRights)
        {
            var cmd = new PSCommand();

            cmd.AddCommand("Add-MailboxPermission");
            cmd.AddParameter("Identity", login);
            cmd.AddParameter("User", loginMailboxRights);
            cmd.AddParameter("AccessRights", "FullAccess");
            cmd.AddParameter("AutoMapping", new SwitchParameter(false));

            ExecutePowershellOffice365WithExchange(cmd);
        }

        public void RemoveMailboxRights(string login, string loginMailboxRights)
        {
            var cmd = new PSCommand();

            cmd.AddCommand("Remove-MailboxPermission");
            cmd.AddParameter("Identity", login);
            cmd.AddParameter("User", loginMailboxRights);
            cmd.AddParameter("AccessRights", "FullAccess");
            cmd.AddParameter("InheritanceType", "All");
            cmd.AddParameter("Confirm", new SwitchParameter(false));

            ExecutePowershellOffice365WithExchange(cmd);
        }

        #endregion MailBoxRights

        #region SendOnBehalfTo

        //public List<SendOnBehalfToItem> GetSendOnBehalfTo(string domain, string login, string servidor, string usuarioAD, string senhaAD, string pathAD)
        public List<string> GetSendOnBehalfTo(string login)
        {
            var retorno = new List<string>();
            PSCommand cmd = new PSCommand();
            cmd.AddCommand("Get-Mailbox");
            cmd.AddParameter("Identity", login);

            var result = ExecutePowershellOffice365WithExchange(cmd);
            if (result != null && result.Count > 0)
            {
                var list = result[0];

                if ((list.Members["GrantSendOnBehalfTo"] != null) && (list.Members["GrantSendOnBehalfTo"].Value != null))
                {
                    var listUsers = ((System.Collections.ArrayList)(((PSObject)list.Members["GrantSendOnBehalfTo"].Value).BaseObject));

                    foreach (var user in listUsers)
                    {
                        retorno.Add(user as string);
                    }
                }
            }

            return retorno;
        }

        public void AddSendOnBehalfTo(string login, string loginSendOnBehalfTo)
        {
            PSCommand cmd = new PSCommand();
            cmd.AddCommand("Set-Mailbox");
            cmd.AddParameter("Identity", login);
            cmd.AddParameter("GrantSendOnBehalfTo", loginSendOnBehalfTo);

            ExecutePowershellOffice365WithExchange(cmd);
        }

        public void RemoveSendOnBehalfTo(string login, string loginSendOnBehalfTo)
        {
            PSCommand cmd = new PSCommand();
            cmd.AddCommand("Set-Mailbox");
            cmd.AddParameter("Identity", login);
            Hashtable userToRemove = new Hashtable();
            userToRemove.Add("remove", loginSendOnBehalfTo);
            cmd.AddParameter("GrantSendOnBehalfTo", userToRemove);
            ExecutePowershellOffice365WithExchange(cmd);
        }

        #endregion SendOnBehalfTo

        #endregion Public Methods

        #region Private Methods

        private SecureString GetSecureString()
        {
            SecureString secpassword = null;

            using (Runspace runspace = RunspaceFactory.CreateRunspace())
            {
                using (PowerShell powershell = PowerShell.Create())
                {
                    runspace.Open();
                    powershell.Runspace = runspace;
                    string script = string.Format("[Byte[]] $key = ({0}); $senha = \"{1}\" | ConvertTo-SecureString -Key $key; $senha;", keyPS, passPS);
                    powershell.AddScript(script);

                    var shellResults = powershell.Invoke();

                    foreach (var res in shellResults)
                    {
                        if (shellResults.Count > 0 && shellResults[0] != null)
                        {
                            secpassword = shellResults[0].BaseObject as SecureString;
                        }
                    }
                }
            }

            return secpassword;
        }        

        private Collection<PSObject> ExecutePowerShellCommandList(string primaryEmail, PSCommand command1, PSCommand command2, PSCommand command3, string servidor, PSCredential credential, List<string> outrosEmails, out List<ErrorRecord> errors)
        {
            errors = new List<ErrorRecord>();
            Collection<PSObject> results = null;
            const string schema = connectionUriOnPremises;

            // Set the connection Info
            var connectionInfo = new WSManConnectionInfo(new Uri(uriServidorOnPremises), schema, credential)
            {
                AuthenticationMechanism = AuthenticationMechanism.Kerberos
            };

            using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
            {
                using (PowerShell powershell = PowerShell.Create())
                {
                    powershell.Commands = command1;

                    try
                    {
                        runspace.Open();
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }

                    powershell.Runspace = runspace;

                    try
                    {
                        results = powershell.Invoke();

                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        if (errors.Count == 0)
                        {
                            powershell.Commands = command2;
                            results = powershell.Invoke();

                            if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                                foreach (ErrorRecord err in powershell.Streams.Error)
                                    errors.Add(err);

                            if (errors.Count == 0)
                            {
                                powershell.Commands = command3;
                                results = powershell.Invoke();

                                if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                                    foreach (ErrorRecord err in powershell.Streams.Error)
                                        errors.Add(err);
                            }
                        }

                        if (outrosEmails != null && outrosEmails.Count > 0)
                        {
                            Hashtable emailCld;
                            PSCommand cmd;

                            foreach (var email in outrosEmails)
                            {
                                emailCld = new Hashtable();
                                emailCld.Add("add", email);
                                cmd = new PSCommand();
                                cmd.AddCommand("Set-RemoteMailbox");
                                cmd.AddParameter("Identity", primaryEmail);
                                cmd.AddParameter("EmailAddresses", emailCld);

                                powershell.Commands = cmd;
                                results = powershell.Invoke();

                                if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                                    foreach (ErrorRecord err in powershell.Streams.Error)
                                        errors.Add(err);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }

                    return results;
                }
            }
        }

        private Collection<PSObject> ExecutePowerShellCommand(PSCommand command, out List<ErrorRecord> errors)
        {
            SecureString secpassword = GetSecureString();
            PSCredential credential = new PSCredential(userPSOnPremises, secpassword);

            errors = new List<ErrorRecord>();
            Collection<PSObject> results = null;

            var connectionInfo = new WSManConnectionInfo(new Uri(uriServidorOnPremises), connectionUriOnPremises, credential)
            {
                AuthenticationMechanism = AuthenticationMechanism.Kerberos
            };

            using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
            {
                using (PowerShell powershell = PowerShell.Create())
                {
                    powershell.Commands = command;

                    try
                    {
                        runspace.Open();
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }

                    powershell.Runspace = runspace;

                    try
                    {
                        results = powershell.Invoke();

                        if (powershell.Streams != null && powershell.Streams.Error != null)
                        {
                            foreach (ErrorRecord err in powershell.Streams.Error)
                            {
                                errors.Add(err);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }

                    return results;
                }
            }
        }

        private Collection<PSObject> ExecutePowerShellCommand(PSCommand command, string servidor, PSCredential credential, out List<ErrorRecord> errors)
        {
            Collection<PSObject> results = null;

            errors = new List<ErrorRecord>();

            var connectionInfo = new WSManConnectionInfo(new Uri(uriServidorOnPremises), connectionUriOnPremises, credential)
            {
                AuthenticationMechanism = AuthenticationMechanism.Kerberos
            };

            using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
            {
                using (PowerShell powershell = PowerShell.Create())
                {
                    powershell.Commands = command;

                    try
                    {
                        runspace.Open();
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }

                    powershell.Runspace = runspace;

                    try
                    {
                        results = powershell.Invoke();

                        if (powershell.Streams != null && powershell.Streams.Error != null)
                        {
                            foreach (ErrorRecord err in powershell.Streams.Error)
                            {
                                errors.Add(err);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }
                }
            }

            return results;
        }

        private Collection<PSObject> ExecutePowershellOffice365WithExchange(PSCommand command)
        {
            PSCredential credential = GetCredentialO365();
            Collection<PSObject> psResults = null;

            var wsEOConnInfo = new WSManConnectionInfo((new Uri(connectionUriO3665WithExchange)), connectionUriOnPremises, credential); // Até 13/05, antes de erro 'Access Denied'
            //var wsEOConnInfo = new WSManConnectionInfo((new Uri(connectionUri)), connectionUriOnPremises, credential);

            //wsEOConnInfo.AuthenticationMechanism = AuthenticationMechanism.Basic;

            //using (Runspace runspace = RunspaceFactory.CreateRunspace(wsEOConnInfo))
            //{
            //    runspace.Open();

            //    if (runspace.RunspaceStateInfo.State == RunspaceState.Opened)
            //    {
            //        PowerShell ps = PowerShell.Create();
            //        ps.Commands = command;
            //        ps.Runspace = runspace;
            //        psResults = ps.Invoke();
            //    }

            //    runspace.Close();
            //}

            wsEOConnInfo.AuthenticationMechanism = AuthenticationMechanism.Basic;

            using (Runspace runspace = RunspaceFactory.CreateRunspace(wsEOConnInfo))
            {
                using (PowerShell ps = PowerShell.Create())
                {
                    ps.Commands = command;
                    runspace.Open();
                    ps.Runspace = runspace;
                    psResults = ps.Invoke();
                }
            }

            return psResults;
        }

        private Collection<PSObject> ExecutePowerShellCommandList(List<PSCommand> commands, out List<ErrorRecord> errors)
        {
            errors = new List<ErrorRecord>();
            Collection<PSObject> results = null;

            SecureString secpassword = GetSecureString();
            PSCredential credential = new PSCredential(userPSOnPremises, secpassword);

            // Set the connection Info
            var connectionInfo = new WSManConnectionInfo(new Uri(uriServidorOnPremises), connectionUriOnPremises, credential)
            {
                AuthenticationMechanism = AuthenticationMechanism.Kerberos
            };

            using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
            {
                using (PowerShell powershell = PowerShell.Create())
                {
                    // open the remote runspace
                    try
                    {
                        runspace.Open();
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }

                    // associate the runspace with powershell
                    powershell.Runspace = runspace;

                    try
                    {
                        foreach (var command in commands)
                        {
                            if (errors.Count == 0)
                            {
                                powershell.Commands = command;
                                results = powershell.Invoke();

                                if (powershell.Streams != null && powershell.Streams.Error != null)
                                {
                                    foreach (ErrorRecord err in powershell.Streams.Error)
                                    {
                                        errors.Add(err);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        if (powershell != null && powershell.Streams != null && powershell.Streams.Error != null)
                            foreach (ErrorRecord err in powershell.Streams.Error)
                                errors.Add(err);

                        throw;
                    }

                    return results;
                }
            }
        }

        #region Verificação E-mails INT e CLD

        private bool ExistsIntEmailAddress(string emailInt)
        {
            var exists = false;
            var errors = new List<ErrorRecord>();

            SecureString secpassword = GetSecureString();
            PSCredential credential = new PSCredential(userPSOnPremises, secpassword);

            PSCommand cmd = new PSCommand();
            //cmd.AddScript(string.Format("Get-RemoteMailbox -Filter {{EmailAddresses -eq '{0}'}}", emailInt));
            cmd.AddCommand("Get-Recipient");
            cmd.AddParameter("Identity", emailInt);

            Collection<PSObject> psResults = null;

            psResults = ExecutePowerShellCommand(cmd, "", credential, out errors);

            // Se e-mail não for encontrado será retornado 'ManagementObjectNotFoundException' em 'errors'.
            var hasNotFoundEx = CheckEmailNotFoundException(errors);

            if (!hasNotFoundEx)
            {
                HandleErrors(errors);

                if (psResults != null && psResults.Count > 0 && psResults[0].Members["RecipientType"] != null &&
                psResults[0].Members["RecipientType"].Value != null && psResults[0].Members["RecipientType"].ToString() != "")
                {
                    exists = true;
                }                
            }

            if (!exists)
            {
                psResults = ExecutePowershellOffice365WithExchange(cmd);

                if (psResults != null && psResults.Count > 0 && psResults[0].Members["RecipientType"] != null &&
                    psResults[0].Members["RecipientType"].Value != null && psResults[0].Members["RecipientType"].ToString() != "")
                {
                    exists = true;
                }
            }

            return exists;
        }

        private bool ExistsCldEmailAddress(string emailCld)
        {
            var exists = false;
            var errors = new List<ErrorRecord>();

            SecureString secpassword = GetSecureString();
            PSCredential credential = new PSCredential(userPSOnPremises, secpassword);

            PSCommand cmd = new PSCommand();
            cmd.AddCommand("Get-Recipient");
            cmd.AddParameter("Identity", emailCld);

            var psResults = ExecutePowerShellCommand(cmd, "", credential, out errors);

            // Se e-mail não for encontrado será retornado 'ManagementObjectNotFoundException' em 'errors'.
            var hasNotFoundEx = CheckEmailNotFoundException(errors);

            if (!hasNotFoundEx)
            {
                HandleErrors(errors);

                if (psResults != null && psResults.Count > 0 && psResults[0].Members["RecipientType"] != null &&
                    psResults[0].Members["RecipientType"].Value != null && psResults[0].Members["RecipientType"].ToString() != "")
                {
                    exists = true;
                }                
            }

            if (!exists)
            {
                psResults = ExecutePowershellOffice365WithExchange(cmd);

                if (psResults != null && psResults.Count > 0 && psResults[0].Members["RecipientType"] != null &&
                    psResults[0].Members["RecipientType"].Value != null && psResults[0].Members["RecipientType"].ToString() != "")
                {
                    exists = true;
                }
            }

            return exists;
        }

        #endregion Verificação E-mails INT e CLD

        private void SplitUpdateEmailList(string identity, List<string> emailList, out string primaryEmail, out List<string> emailsToAdd, out List<string> emailsToRemove)
        {
            var allEmailsList = GetUserEmails(identity);
            allEmailsList = GetRemovedSmtpPrefixFromEmails(allEmailsList).ToList();
            primaryEmail = allEmailsList[0];

            var splitResult = primaryEmail.Split(new char[] { '@' });
            var domain = splitResult[1];
            var cldEmail = BuildCldEmailAddress(identity);
            var intEmail = BuildIntEmailAddress(identity, domain);

            var emailListToLower = emailList.Select(x => x?.ToLower()).ToList();

            // Remove e-mail primário, que não será atualizado
            allEmailsList.RemoveAt(0);
            allEmailsList = allEmailsList.Select(x => x?.ToLower()).Where(x => (x != cldEmail) && (x != intEmail)).ToList();

            var currentEmailList = allEmailsList.Select(x => x?.ToLower()).ToList();
            emailsToRemove = currentEmailList.Where(x => !emailListToLower.Contains(x)).ToList();
            emailsToAdd = emailListToLower.Where(x => !currentEmailList.Contains(x)).ToList();
        }

        private void HandleErrors(List<ErrorRecord> errors)
        {
            if (errors.Count > 0)
            {
                if (errors[0].ErrorDetails != null)
                {
                    throw new Exception(errors[0].ErrorDetails.Message);
                }
                else if (errors[0].Exception != null)
                {
                    throw new Exception(errors[0].Exception.Message);
                }
            }
        }        

        private PSCredential GetCredentialO365()
        {
            SecureString secpassword = GetSecureString();
            PSCredential credential = new PSCredential(userPS, secpassword);

            return credential;
        }

        private List<string> GetRemovedSmtpPrefixFromEmails(List<string> emailList)
        {
            return emailList.Select(email => email.Replace("SMTP:", string.Empty).Replace("smtp:", string.Empty)).ToList();
        }

        private bool CheckEmailNotFoundException(List<ErrorRecord> errors)
        {
            var result = false;

            if (errors.Count > 0 && errors[0]?.CategoryInfo.Reason == "ManagementObjectNotFoundException")
            {
                result = true;
            }

            return result;
        }

        private string BuildCldEmailAddress(string identity)
        {
            var cldEmail = string.Format("{0}@gerdaucld.mail.onmicrosoft.com", identity).ToLower();            

            return cldEmail;
        }

        private string BuildIntEmailAddress(string identity, string domain)
        {
            var intEmail = string.Format("{0}@int.{1}", identity, domain).ToLower();

            return intEmail;
        }

        private Collection<PSObject> ExecuteCommandPowershellOffice365(PSCommand command)
        {
            SecureString secpassword = null;
            Collection<PSObject> results;

            try
            {
                InitialSessionState initial = InitialSessionState.CreateDefault();
                initial.ImportPSModule(new string[] { "MSOnline" });
                using (Runspace runspace = RunspaceFactory.CreateRunspace(initial))
                {
                    using (PowerShell pshell = PowerShell.Create())
                    {
                        secpassword = GetSecureString();
                        PSCommand commandAux = new PSCommand();
                        PSCredential credential = new PSCredential(userPS, secpassword);
                        commandAux.AddCommand("New-PSSession");
                        commandAux.AddParameter("Credential", credential);
                        commandAux.AddParameter("Authentication", "Basic");
                        commandAux.AddParameter("ConnectionUri", new Uri(connectionUri));
                        commandAux.AddParameter("AllowRedirection");
                        commandAux.AddParameter("ConfigurationName", "Microsoft.Exchange");
                        commandAux.AddParameter("Verbose");
                        pshell.Commands = commandAux;
                        runspace.Open();
                        pshell.Runspace = runspace;

                        Collection<PSObject> result = pshell.Invoke();

                        CheckOffice365Error(pshell, "Failed to establish the connection: ");

                        commandAux = new PSCommand();
                        commandAux.AddCommand("Connect-MsolService");
                        commandAux.AddParameter("Credential", credential);
                        pshell.Commands = commandAux;
                        result = pshell.Invoke();

                        CheckOffice365Error(pshell);

                        pshell.Commands = command;
                        results = pshell.Invoke();

                        CheckOffice365Error(pshell);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (secpassword != null)
                {
                    secpassword.Dispose();
                }
            }

            return results;
        }

        private void CheckOffice365Error(PowerShell pshell, string message = null)
        {
            if (pshell.Streams.Error.Count > 0)
            {
                if (message == null)
                {
                    message = "Error occurred when executing command: ";
                }

                if (pshell.Streams.Error[0].ErrorDetails != null)
                {
                    throw new Exception(message + pshell.Streams.Error[0].ErrorDetails.Message);
                }
                else
                {
                    throw new Exception(message + pshell.Streams.Error[0].Exception.Message);
                }

            }
        }
        
        #endregion Private Methods

        #region remover depois
        //    private Collection<PSObject> ExecutePowerShellCommand(PSCommand command, string servidor, Runspace rspace, PowerShell pshell,  out List<ErrorRecord> errors)
        //    {
        //        try
        //        {
        //            errors = new List<ErrorRecord>();
        //            pshell.Commands = command;
        //            pshell.Runspace = rspace;

        //            try
        //            {
        //                // invoke the powershell to obtain the results
        //                var r = pshell.Invoke();

        //                if (pshell.Streams != null && pshell.Streams.Error != null)
        //                    foreach (ErrorRecord err in pshell.Streams.Error)
        //                        errors.Add(err);

        //                return r;
        //            }
        //            catch (Exception)
        //            {
        //                if (pshell.Streams != null && pshell.Streams.Error != null)
        //                    foreach (ErrorRecord err in pshell.Streams.Error)
        //                        errors.Add(err);

        //                throw exInvoke;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }        

        //    private Collection<PSObject> ExecuteCommandPowershellOffice365(PSCommand command)
        //    {
        //        SecureString secpassword = null;
        //        Collection<PSObject> results;

        //        try
        //        {
        //            InitialSessionState initial = InitialSessionState.CreateDefault();
        //            initial.ImportPSModule(new string[] { "MSOnline" });
        //            using (Runspace runspace = RunspaceFactory.CreateRunspace(initial))
        //            {
        //                using (PowerShell pshell = PowerShell.Create())
        //                {
        //                    secpassword = GetSecureString();
        //                    PSCommand commandAux = new PSCommand();
        //                    PSCredential credential = new PSCredential(userPS, secpassword);
        //                    commandAux.AddCommand("New-PSSession");
        //                    commandAux.AddParameter("Credential", credential);
        //                    commandAux.AddParameter("Authentication", "Basic");
        //                    commandAux.AddParameter("ConnectionUri", new Uri(connectionUri));
        //                    commandAux.AddParameter("AllowRedirection");
        //                    commandAux.AddParameter("ConfigurationName", "Microsoft.Exchange");
        //                    commandAux.AddParameter("Verbose");
        //                    pshell.Commands = commandAux;
        //                    runspace.Open();
        //                    pshell.Runspace = runspace;

        //                    Collection<PSObject> result = pshell.Invoke();

        //                    CheckOffice365Error(pshell, "Failed to establish the connection: ");

        //                    commandAux = new PSCommand();
        //                    commandAux.AddCommand("Connect-MsolService");
        //                    commandAux.AddParameter("Credential", credential);
        //                    pshell.Commands = commandAux;
        //                    result = pshell.Invoke();

        //                    CheckOffice365Error(pshell);

        //                    pshell.Commands = command;
        //                    results = pshell.Invoke();

        //                    CheckOffice365Error(pshell);
        //                }
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //        finally
        //        {
        //            if (secpassword != null)
        //            {
        //                secpassword.Dispose();
        //            }
        //        }

        //        return results;
        //    }

        //    private void AddMsgs(StringBuilder msgs, string msg)
        //    {
        //        if (msgs != null)
        //        {
        //            msgs.AppendLine(msg + '\n');
        //        }
        //    }        


        //    #region Comandos Office 365

        //    public bool HasMailboxInOffice365(string displayName)
        //    {
        //        PSCommand cmd = new PSCommand();
        //        var userExists = false;
        //        cmd.AddCommand("Get-MsolUser");
        //        cmd.AddParameter("SearchString", displayName);

        //        Collection<PSObject> results = ExecuteCommandPowershellOffice365(cmd);

        //        if (results != null && results.Count > 0)
        //        {
        //            var name = results[0].Properties["DisplayName"];
        //            var isLicensed = results[0].Properties["IsLicensed"];

        //            if ((name != null && Convert.ToString(name.Value) != string.Empty) &&
        //                (isLicensed != null && Convert.ToBoolean(isLicensed.Value)))
        //            {
        //                userExists = true;
        //            }
        //        }

        //        return userExists;
        //    }        

        //    #endregion

        //    #region Comandos Exchange On Premises        


        //    #endregion

        //    private void CheckOffice365Error(PowerShell pshell, string message = null)
        //    {
        //        if (pshell.Streams.Error.Count > 0)
        //        {
        //            if (message == null)
        //            {
        //                message = "Error occurred when executing command: ";
        //            }

        //            if (pshell.Streams.Error[0].ErrorDetails != null)
        //            {
        //                throw new Exception(message + pshell.Streams.Error[0].ErrorDetails.Message);
        //            }
        //            else
        //            {
        //                throw new Exception(message + pshell.Streams.Error[0].Exception.Message);
        //            }

        //        }
        //    }

        //    public bool DeleteMailbox(string login, string upn, out string errorMessage)
        //    {
        //        var success = false;
        //        List<ErrorRecord> errors;

        //        errorMessage = "";
        //        RemoveMailboxInOffice365(upn); 
        //        RemoveMailboxInExchangeOnPremises(login, out errors);

        //        if (errors.Count > 0)
        //        {
        //            foreach (var item in errors)
        //            {
        //                errorMessage += " " + item.ToString();
        //            }

        //            success = false;
        //        }
        //        else
        //        {
        //            success = true;
        //        }

        //        return success;
        //    }

        //    private Collection<PSObject> RemoveMailboxInOffice365(string upn)
        //    {
        //        var cmd = new PSCommand();
        //        cmd.AddCommand("Remove-MsolUser");
        //        cmd.AddParameter("UserPrincipalName", upn);
        //        cmd.AddParameter("Force");

        //        var results = ExecuteCommandPowershellOffice365(cmd);

        //        return results;
        //    }        

        //    #region Mailbox Rights

               
        //    #endregion Mailbox Rights

        //    #region Send On Behalf To        

        //    private string GetPrimaryEmailFromMailboxOffice365(string id)
        //    {
        //        try
        //        {
        //            var primaryEmail = "";
        //            var cmd = new PSCommand();
        //            cmd.AddCommand("Get-Mailbox");
        //            cmd.AddParameter("Identity", id);

        //            var results = ExecutePowershellOffice365WithExchange(cmd);

        //            if (results != null && results.Count > 0 && results[0].Members["PrimarySmtpAddress"] != null && results[0].Members["PrimarySmtpAddress"].Value != null)
        //            {
        //                primaryEmail = results[0].Members["PrimarySmtpAddress"].Value as string;
        //            }

        //            return primaryEmail;
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }
        
        //    #endregion Send On Behalf To

        //    #region Send As

        //    public Collection<PSObject> GetSendAsList(string domain, string login, string servidor, string usuarioAD, string senhaAD, string pathAD)
        //    {
        //        Collection<PSObject> usuarios;

        //        try
        //        {
        //            var command = new PSCommand();
        //            command.AddCommand("Get-RecipientPermission");
        //            command.AddParameter("Identity", login);

        //            usuarios = ExecutePowershellOffice365WithExchange(command);
        //        }
        //        catch (Exception)
        //        {

        //            throw;
        //        }

        //        return usuarios;
        //    }        

        

        //    #endregion Send As        


        //    #region GetCredential Exchange e Office365

        //    private PSCredential GetCredentialExchange()
        //    {
        //        SecureString secpassword = GetSecureString();
        //        PSCredential credential = new PSCredential(userPSOnPremises, secpassword);

        //        return credential;
        //    }



        //    #endregion

        //    private PSCredential GetCredential()
        //    {
        //        SecureString secpassword = GetSecureString();
        //        PSCredential credential = new PSCredential(userPSOnPremises, secpassword);

        //        return credential;
        //    }

        //    private string GetErrorMessage(List<ErrorRecord> errors)
        //    {
        //        var errorMessage = "";

        //        foreach (var item in errors)
        //        {
        //            errorMessage += " " + item.ToString();
        //        }

        //        return errorMessage;
        //    }
        #endregion
    }
}
