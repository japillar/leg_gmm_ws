﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Management.Automation;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// TODO: Tratar localização
namespace GmmWebService
{  
    class PowershellConfig
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Key { get; set; }
        public string UserOnPremises { get; set; }
        public string ServidorOnPremises { get; set; }
    }

    public class GmmWs : IGmmWs
    {
        public GmmWsGetUserEmailsResult GetUserEmails(string identity)
        {
            GmmWsGetUserEmailsResult result = new GmmWsGetUserEmailsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                var emailList = pso.GetUserEmails(identity);

                result.Data = emailList;
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        public GmmWsResult CreateMailbox(string login, string email, string domain, List<string> outrosEmails)
        {
            string errorMessage;
            var wsResult = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                var result = pso.CreateMailbox(login, email, domain, outrosEmails, out errorMessage);

                if (!result)
                {
                    wsResult.HasError = true;
                    wsResult.ErrorMessage = errorMessage;
                }
                else
                {                    
                    wsResult.OperationResultMessage = "E-mail created successfully";
                }
            }
            catch (Exception ex)
            {
                wsResult.HasError = true;
                wsResult.ErrorMessage = ex.Message;
            }

            return wsResult;
        }

        public GmmWsResult UpdateEmailList(string identity, List<string> emailList)
        {
            string updateMessage = "";
            string errorMessage = "";            
            var wsResult = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                var result = pso.UpdateEmailList(identity, emailList, out updateMessage, out errorMessage);

                if (!result)
                {
                    wsResult.HasError = true;
                    wsResult.ErrorMessage = errorMessage;
                }
                else
                {
                    wsResult.OperationResultMessage = updateMessage;
                }
            }
            catch (Exception ex)
            {
                wsResult.HasError = true;
                wsResult.ErrorMessage = ex.Message;
            }

            return wsResult;
        }

        public GmmWsResult RemoveMailboxInExchangeOnPremises(string identity)
        {
            List<ErrorRecord> errors = new List<ErrorRecord>();
            var result = new GmmWsResult();
            
            try
            {
                var pso = CreatePowershellOffice365();
                pso.RemoveMailboxInExchangeOnPremises(identity, out errors);

                if (errors.Count > 0)
                {
                    var errMsg = "";

                    foreach (var err in errors)
                    {
                        errMsg += err.Exception.Message;
                    }

                    result.ErrorMessage = errMsg;
                }
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        #region SendAs Operations

        public GetSendAsWsResponse GetSendAsList(string login)
        {
            var result = new GetSendAsWsResponse(); 

            try
            {
                var pso = CreatePowershellOffice365();
                result.SendAsList = pso.GetSendAsList(login);
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        public GmmWsResult AddSendAs(string login, string loginSendAs)
        {
            var result = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                pso.AddSendAs(login, loginSendAs);
                result.OperationResultMessage = $"SendAs permission of {loginSendAs} added successfully to {login}.";
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        public GmmWsResult RemoveSendAs(string login, string loginSendAs)
        {
            var result = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                pso.RemoveSendAs(login, loginSendAs);
                result.OperationResultMessage = $"SendAs permission of {loginSendAs} removed successfully from {login}.";
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        #endregion SendAs Operations

        #region Mailbox Rights Operations
        public GetMailBoxRightsResponse GetMailboxRights(string login)
        {
            var result = new GetMailBoxRightsResponse();

            try
            {
                var pso = CreatePowershellOffice365();
                result.MailBoxRightsList = pso.ListMailboxRights(login);
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        public GmmWsResult AddMailboxRights(string login, string loginMailboxRights)
        {
            var result = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                pso.AddMailboxRights(login, loginMailboxRights);
                result.OperationResultMessage = $"MailboxRights permission of {loginMailboxRights} added successfully to {login}.";
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        public GmmWsResult RemoveMailboxRights(string login, string loginMailboxRights)
        {
            var result = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                pso.RemoveMailboxRights(login, loginMailboxRights);
                result.OperationResultMessage = $"MailboxRights permission of {loginMailboxRights} removed successfully from {login}.";
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        #endregion Mailbox Rights Operations

        #region SendOnBehalfTo Operations
        public GetSendOnBehalfToResponse GetSendOnBehalfTo(string login)
        {
            var result = new GetSendOnBehalfToResponse();

            try
            {
                var pso = CreatePowershellOffice365();
                result.SendOnBehalfToList = pso.GetSendOnBehalfTo(login);
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        public GmmWsResult AddSendOnBehalfTo(string login, string loginSendOnBehalfTo)
        {
            var result = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                pso.AddSendOnBehalfTo(login, loginSendOnBehalfTo);
                result.OperationResultMessage = $"SendOnBehalfTo permission of {loginSendOnBehalfTo} added successfully to {login}.";
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }

        public GmmWsResult RemoveSendOnBehalfTo(string login, string loginSendOnBehalfTo)
        {
            var result = new GmmWsResult();

            try
            {
                var pso = CreatePowershellOffice365();
                pso.RemoveSendOnBehalfTo(login, loginSendOnBehalfTo);
                result.OperationResultMessage = $"SendOnBehalfTo permission of {loginSendOnBehalfTo} removed successfully from {login}.";
            }
            catch (Exception ex)
            {
                result.HasError = true;
                result.ErrorMessage = $"Message: {ex.Message}\n Stack Trace: {ex.StackTrace}";

                if (ex.InnerException != null)
                {
                    result.ErrorMessage += $"\nInner Exception: Message: {ex.InnerException.Message}\n Stack Trace: {ex.InnerException.StackTrace}";
                }
            }

            return result;
        }
        #endregion SendOnBehalfTo Operations

        #region Private Methods
        private PowershellOffice365 CreatePowershellOffice365()
        {
            var cred = GetCredentialsConfig();
            var pso = new PowershellOffice365(cred.User, cred.Password, "", cred.Key, cred.UserOnPremises, cred.ServidorOnPremises);

            return pso;
        }

        private PowershellConfig GetCredentialsConfig()
        {
            var cred = new PowershellConfig
            {
                User = ConfigurationManager.AppSettings["UsuarioOffice365"],
                UserOnPremises = ConfigurationManager.AppSettings["UsuarioOnPremises"],
                Password = ConfigurationManager.AppSettings["SenhaOffice365"],
                Key = ConfigurationManager.AppSettings["ChaveOffice365"],
                ServidorOnPremises = ConfigurationManager.AppSettings["ServidorOnPremises"]
            };

            if (cred.User == null)
            {
                throw new Exception("UsuarioOffice365 configuration does not exist in Web.config.");
            }

            if (cred.User.IsEmpty())
            {
                throw new Exception("UsuarioOffice365 configuration is empty in Web.config.");
            }

            if (cred.UserOnPremises == null)
            {
                throw new Exception("UsuarioOnPremises configuration does not exist in Web.config.");
            }

            if (cred.UserOnPremises.IsEmpty())
            {
                throw new Exception("UsuarioOnPremises configuration is empty in Web.config.");
            }

            if (cred.Password == null)
            {
                throw new Exception("SenhaOffice365 configuration does not exist in Web.config.");
            }

            if (cred.Password.IsEmpty())
            {
                throw new Exception("SenhaOffice365 configuration is empty in Web.config.");
            }

            if (cred.Key == null)
            {
                throw new Exception("ChaveOffice365 configuration does not exist in Web.config.");
            }

            if (cred.Key.IsEmpty())
            {
                throw new Exception("ChaveOffice365 configuration is empty in Web.config.");
            }

            if (cred.ServidorOnPremises == null)
            {
                throw new Exception("ServidorOnPremises configuration does not exist in Web.config.");
            }

            if (cred.ServidorOnPremises.IsEmpty())
            {
                throw new Exception("ServidorOnPremises configuration is empty in Web.config.");
            }

            return cred;
        } 
        #endregion
    }
}
