﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GmmWebService
{
    public static class ExtensionMethods
    {
        public static bool IsBlank(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static bool IsEmpty(this string str)
        {
            return str != null && str == "";
        }
    }
}