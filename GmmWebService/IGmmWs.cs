﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GmmWebService
{
    [ServiceContract]
    public interface IGmmWs
    {
        [OperationContract]
        GmmWsGetUserEmailsResult GetUserEmails(string identity);

        [OperationContract]
        GmmWsResult CreateMailbox(string login, string email, string domain, List<string> outrosEmails);

        [OperationContract]
        GmmWsResult UpdateEmailList(string identity, List<string> emailList);

        [OperationContract]
        GmmWsResult RemoveMailboxInExchangeOnPremises(string identity);

        [OperationContract]
        GetSendAsWsResponse GetSendAsList(string login);

        [OperationContract]
        GmmWsResult AddSendAs(string login, string loginSendAs);

        [OperationContract]
        GmmWsResult RemoveSendAs(string login, string loginSendAs);

        [OperationContract]
        GetMailBoxRightsResponse GetMailboxRights(string login);

        [OperationContract]
        GmmWsResult AddMailboxRights(string login, string loginMailboxRights);

        [OperationContract]
        GmmWsResult RemoveMailboxRights(string login, string loginMailboxRights);

        [OperationContract]
        GetSendOnBehalfToResponse GetSendOnBehalfTo(string login);

        [OperationContract]
        GmmWsResult AddSendOnBehalfTo(string login, string loginSendOnBehalfTo);

        [OperationContract]
        GmmWsResult RemoveSendOnBehalfTo(string login, string loginSendOnBehalfTo);
    }

    [DataContract]
    public class GmmWsResult
    {
        [DataMember]
        public bool HasError { get; set; }

        [DataMember]
        public string OperationResultMessage { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        public GmmWsResult()
        {
            ErrorMessage = "";
            HasError = false;
            OperationResultMessage = "";
        }
    }

    [DataContract]
    public class GmmWsGetUserEmailsResult : GmmWsResult
    {
        [DataMember]
        public List<string> Data { get; set; }

        public GmmWsGetUserEmailsResult()
        {
            Data = new List<string>();
        }
    }

    [DataContract]
    public class PsSendAsItem
    {
        [DataMember]
        public string Trustee { get; set; }
        [DataMember]
        public string AccessRights { get; set; }
        [DataMember]
        public string AccessControlType { get; set; }
        [DataMember]
        public bool IsInherited { get; set; }
        [DataMember]
        public string Identity { get; set; }
    }

    public class GetSendAsWsResponse
    {
        [DataMember]
        public bool HasError { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public List<PsSendAsItem> SendAsList { get; set; }

        public GetSendAsWsResponse()
        {
            HasError = false;
            ErrorMessage = "";
        }
    }

    [DataContract]
    public class PsMailBoxRightsItem
    {
        [DataMember]
        public string User { get; set; }
        [DataMember]
        public string AccessRights { get; set; }
        [DataMember]
        public string Deny { get; set; }
        [DataMember]
        public bool HasFullAccess { get; set; }
        [DataMember]
        public bool IsInherited { get; set; }
        [DataMember]
        public string Identity { get; set; }
    }

    [DataContract]
    public class GetMailBoxRightsResponse
    {
        [DataMember]
        public bool HasError { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public List<PsMailBoxRightsItem> MailBoxRightsList { get; set; }

        public GetMailBoxRightsResponse()
        {
            HasError = false;
            ErrorMessage = "";
        }
    }

    //[DataContract]
    //public class PsSendOnBehalfToItem
    //{
    //    [DataMember]
    //    public string Name { get; set; }
    //    [DataMember]
    //    public string DistinguishedName { get; set; }
    //    [DataMember]
    //    public string Login { get; set; }
    //}

    public class GetSendOnBehalfToResponse
    {
        [DataMember]
        public bool HasError { get; set; }


        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public List<string> SendOnBehalfToList { get; set; }

        public GetSendOnBehalfToResponse()
        {
            HasError = false;
            ErrorMessage = "";
        }
    }
}
