﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;

namespace GmmWebService
{
    public class SendOnBehalfToItem
    {
        public string Name { get; set; }
        public string DistinguishedName { get; set; }
        public string Login { get; set; }
    }

    public class SendOnBehalfTo
    {
        private string Path { get; set; }
        private string UserAD { get; set; }
        private string PassAD { get; set; }
        private string UserPS { get; set; }
        private string PassPS { get; set; }
        private string UserPSO365 { get; set; }
        private string PassPSO365 { get; set; }
        private string KeyO365 { get; set; }
        private string UserPSO365_2 { get; set; }

        public SendOnBehalfTo(string path, string userAD, string passAD, string userPS, string passPS, string userPsO365 = "", string passPsO365 = "", string keyPsO365 = "", string userPSO365_2 = "")
        {
            Path = path;
            UserAD = userAD;
            PassAD = passAD;
            UserPS = userPS;
            PassPS = passPS;
            UserPSO365 = userPsO365;
            PassPSO365 = passPsO365;
            KeyO365 = keyPsO365;
            UserPSO365_2 = userPSO365_2;
        }

        public List<SendOnBehalfToItem> Get(string login, string homeMdbURL, string server)
        {
            List<SendOnBehalfToItem> results = null;

            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(homeMdbURL) && !string.IsNullOrEmpty(server))
            {
                var arrHomeMdb = homeMdbURL.Split(new string[] { ",DC=" }, StringSplitOptions.None);
                var domain = string.Format("{0}.{1}", arrHomeMdb[1], arrHomeMdb[2]);
                PowershellOffice365 ps = new PowershellOffice365(UserPSO365, PassPSO365, homeMdbURL, KeyO365, UserPSO365_2);
                results = ps.GetSendOnBehalfTo(domain, login, server, UserAD, PassAD, Path);
            }

            return results;
        }

        public string Add(string login, string loginSendOnBehalfTo, string homeMdbURL, string server) 
        {
            var retorno = "";

            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(homeMdbURL) && !string.IsNullOrEmpty(server))
            {
                AD ad = new AD(UserAD, PassAD, Path);
                DirectoryEntry userSendOnBehalfTo = ad.GetAccount(loginSendOnBehalfTo);
                retorno = userSendOnBehalfTo.Properties["displayName"].Value != null ? userSendOnBehalfTo.Properties["displayName"].Value.ToString() : "";

                var arrHomeMdb = homeMdbURL.Split(new string[] { ",DC=" }, StringSplitOptions.None);
                var domain = string.Format("{0}.{1}", arrHomeMdb[1], arrHomeMdb[2]);
                PowershellOffice365 ps = new PowershellOffice365(UserPSO365, PassPSO365, homeMdbURL, KeyO365, UserPSO365_2);
                ps.AddSendOnBehalfTo(domain, login, loginSendOnBehalfTo, server);
            }

            return retorno;
        }

        public void Delete(string login, string loginSendOnBehalfTo, string homeMdbURL, string server)
        {
            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(homeMdbURL) && !string.IsNullOrEmpty(server))
            {
                var arrHomeMdb = homeMdbURL.Split(new string[] { ",DC=" }, StringSplitOptions.None);
                var domain = string.Format("{0}.{1}", arrHomeMdb[1], arrHomeMdb[2]);
                PowershellOffice365 ps = new PowershellOffice365(UserPSO365, PassPSO365, homeMdbURL, KeyO365, UserPSO365_2);
                ps.RemoveSendOnBehalfTo(domain, login, loginSendOnBehalfTo, server);
            }
        }
    }
}
